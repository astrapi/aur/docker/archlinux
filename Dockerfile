ARG ARCH_1=amd64
ARG ARCH_2=x86_64
FROM archlinux:$ARCH_2
#FROM lopsided/archlinux-$ARCH_1

WORKDIR /archlinux

RUN mkdir -p /archlinux/rootfs

COPY pacstrap-docker /archlinux/

RUN ./pacstrap-docker /archlinux/rootfs \
      bash sed gzip pacman archlinux-keyring && \
    # Install Arch Linux ARM keyring if available
    (pacman -r /archlinux/rootfs -S --noconfirm archlinuxarm-keyring || true) && \
    # Remove current pacman database, likely outdated very soon
    rm rootfs/var/lib/pacman/sync/*

FROM scratch
ARG ARCH_1=amd64

COPY --from=0 /archlinux/rootfs/ /
COPY rootfs/common/ /
COPY rootfs/$ARCH_1/ /

ENV LANG=en_US.UTF-8

RUN cat /etc/pacman.d/mirrorlist

RUN locale-gen && \
    pacman-key --init && \
    pacman-key --populate archlinux && \
    (pacman-key --populate archlinuxarm || true)

CMD ["/usr/bin/bash"]
